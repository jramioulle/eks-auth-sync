module gitlab.com/polarsquad/eks-auth-sync

go 1.14

require (
	github.com/aws/aws-sdk-go v1.33.12
	github.com/spf13/afero v1.3.2
	github.com/stretchr/testify v1.5.1
	gopkg.in/yaml.v2 v2.3.0
	k8s.io/api v0.17.9
	k8s.io/apimachinery v0.17.9
	k8s.io/client-go v0.17.9
)
